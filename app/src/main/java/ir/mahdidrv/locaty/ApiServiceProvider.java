package ir.mahdidrv.locaty;

public class ApiServiceProvider {
    private static ApiService apiService;

    public static ApiService serviceProvider() {
        if (apiService == null){
            apiService =RetrofitSingleton.getInstance().create(ApiService.class);
        }
        return apiService;
    }
}
