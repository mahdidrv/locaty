package ir.mahdidrv.locaty.model.detail;

import java.util.List;

public class GroupsItem{
	private String summary;
	private String name;
	private int count;
	private String type;
	private List<ItemsItem> items;
}