package ir.mahdidrv.locaty.model.venue;

public class CategoriesItem{
	private String pluralName;
	private String name;
	private Icon icon;
	private String id;
	private String shortName;
	private boolean primary;
}
