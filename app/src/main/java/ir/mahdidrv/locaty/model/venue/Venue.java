package ir.mahdidrv.locaty.model.venue;

import java.util.List;

public class Venue{
	private Meta meta;
	private Response response;
	private String name;
	private Location location;
	private String id;
	private List<CategoriesItem> categories;
	private Photos photos;

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<CategoriesItem> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoriesItem> categories) {
		this.categories = categories;
	}

	public Photos getPhotos() {
		return photos;
	}

	public void setPhotos(Photos photos) {
		this.photos = photos;
	}
}
