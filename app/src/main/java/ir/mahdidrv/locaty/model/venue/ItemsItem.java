package ir.mahdidrv.locaty.model.venue;

public class ItemsItem{
	private Venue venue;
	private Reasons reasons;
	private String referralId;
	private String summary;
	private String reasonName;
	private String type;


	public Venue getVenue() {
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}

	public Reasons getReasons() {
		return reasons;
	}

	public void setReasons(Reasons reasons) {
		this.reasons = reasons;
	}

	public String getReferralId() {
		return referralId;
	}

	public void setReferralId(String referralId) {
		this.referralId = referralId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getReasonName() {
		return reasonName;
	}

	public void setReasonName(String reasonName) {
		this.reasonName = reasonName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
