package ir.mahdidrv.locaty.model.detail;

import java.util.List;

public class Location{
	private String cc;
	private String country;
	private String address;
	private List<LabeledLatLngsItem> labeledLatLngs;
	private double lng;
	private List<String> formattedAddress;
	private String city;
	private String neighborhood;
	private String state;
	private String crossStreet;
	private double lat;
}