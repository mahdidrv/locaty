package ir.mahdidrv.locaty.model.venue;

import java.util.List;

public class Location{
	private String cc;
	private String country;
	private String address;
	private List<LabeledLatLngsItem> labeledLatLngs;
	private double lng;
	private int distance;
	private List<String> formattedAddress;
	private String city;
	private String state;
	private double lat;
	private String crossStreet;
	private String postalCode;

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<LabeledLatLngsItem> getLabeledLatLngs() {
		return labeledLatLngs;
	}

	public void setLabeledLatLngs(List<LabeledLatLngsItem> labeledLatLngs) {
		this.labeledLatLngs = labeledLatLngs;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public List<String> getFormattedAddress() {
		return formattedAddress;
	}

	public void setFormattedAddress(List<String> formattedAddress) {
		this.formattedAddress = formattedAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public String getCrossStreet() {
		return crossStreet;
	}

	public void setCrossStreet(String crossStreet) {
		this.crossStreet = crossStreet;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
}