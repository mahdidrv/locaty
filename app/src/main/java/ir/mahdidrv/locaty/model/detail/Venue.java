package ir.mahdidrv.locaty.model.detail;

import java.util.List;

public class Venue{
	private Reasons reasons;
	private Specials specials;
	private boolean dislike;
	private String shortUrl;
	private double rating;
	private Photos photos;
	private Tips tips;
	private Colors colors;
	private HereNow hereNow;
	private int createdAt;
	private Stats stats;
	private Contact contact;
	private String ratingColor;
	private String id;
	private List<CategoriesItem> categories;
	private boolean ok;
	private boolean allowMenuUrlEdit;
	private Popular popular;
	private Likes likes;
	private Hours hours;
	private String canonicalUrl;
	private boolean verified;
	private String timeZone;
	private String url;
	private BeenHere beenHere;
	private BestPhoto bestPhoto;
	private int ratingSignals;
	private Listed listed;
	private String name;
	private Location location;
	private Attributes attributes;
	private PageUpdates pageUpdates;
	private Inbox inbox;
}