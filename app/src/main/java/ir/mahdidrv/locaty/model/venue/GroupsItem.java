package ir.mahdidrv.locaty.model.venue;

import java.util.List;

public class GroupsItem{
	private String name;
	private String type;
	private List<ItemsItem> items;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<ItemsItem> getItems() {
		return items;
	}

	public void setItems(List<ItemsItem> items) {
		this.items = items;
	}
}