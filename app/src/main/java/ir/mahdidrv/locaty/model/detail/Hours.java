package ir.mahdidrv.locaty.model.detail;

import java.util.List;

public class Hours{
	private boolean isOpen;
	private List<TimeframesItem> timeframes;
	private List<Object> dayData;
	private RichStatus richStatus;
	private boolean isLocalHoliday;
	private String status;
}