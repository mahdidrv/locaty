package ir.mahdidrv.locaty.model.venue;

import java.util.List;

public class Response{
	private SuggestedFilters suggestedFilters;
	private int totalResults;
	private int suggestedRadius;
	private String headerFullLocation;
	private String headerLocationGranularity;
	private List<GroupsItem> groups;
	private SuggestedBounds suggestedBounds;
	private String headerLocation;

	public SuggestedFilters getSuggestedFilters() {
		return suggestedFilters;
	}

	public void setSuggestedFilters(SuggestedFilters suggestedFilters) {
		this.suggestedFilters = suggestedFilters;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public int getSuggestedRadius() {
		return suggestedRadius;
	}

	public void setSuggestedRadius(int suggestedRadius) {
		this.suggestedRadius = suggestedRadius;
	}

	public String getHeaderFullLocation() {
		return headerFullLocation;
	}

	public void setHeaderFullLocation(String headerFullLocation) {
		this.headerFullLocation = headerFullLocation;
	}

	public String getHeaderLocationGranularity() {
		return headerLocationGranularity;
	}

	public void setHeaderLocationGranularity(String headerLocationGranularity) {
		this.headerLocationGranularity = headerLocationGranularity;
	}

	public List<GroupsItem> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupsItem> groups) {
		this.groups = groups;
	}

	public SuggestedBounds getSuggestedBounds() {
		return suggestedBounds;
	}

	public void setSuggestedBounds(SuggestedBounds suggestedBounds) {
		this.suggestedBounds = suggestedBounds;
	}

	public String getHeaderLocation() {
		return headerLocation;
	}

	public void setHeaderLocation(String headerLocation) {
		this.headerLocation = headerLocation;
	}
}