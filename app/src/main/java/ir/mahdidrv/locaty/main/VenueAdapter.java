package ir.mahdidrv.locaty.main;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.mahdidrv.locaty.R;
import ir.mahdidrv.locaty.model.venue.Venue;

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.VenueViewHolder> {

    List<Venue> venues;

    public VenueAdapter() {

    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }

    @NonNull
    @Override
    public VenueViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VenueViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_venue, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VenueViewHolder holder, int position) {
        holder.bindVenue(venues.get(position));
    }

    @Override
    public int getItemCount() {
        return venues.size();
    }

    public class VenueViewHolder extends RecyclerView.ViewHolder {

        TextView titleTv;
        TextView distanceTv;

        public VenueViewHolder(@NonNull View view) {
            super(view);

            titleTv = view.findViewById(R.id.txt_itemVenue_title);
            distanceTv = view.findViewById(R.id.txt_itemVenue_distance);
        }

        public void bindVenue(Venue venue) {
            titleTv.setText(String.valueOf(venue.getName()));
            distanceTv.setText(String.valueOf(venue.getLocation().getDistance()));
        }
    }
}
