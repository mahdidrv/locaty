package ir.mahdidrv.locaty.main;

import io.reactivex.Single;
import ir.mahdidrv.locaty.ApiService;
import ir.mahdidrv.locaty.ApiServiceProvider;
import ir.mahdidrv.locaty.model.detail.Detail;
import ir.mahdidrv.locaty.model.venue.Venue;

public class MainViewModel {
    private ApiService apiService = ApiServiceProvider.serviceProvider();


    // TODO: clientId and clientSecret convert to Static method?
    public Single<Venue> getRecommended(String clientId, String clientSecret, int v, String latLng) {
        return apiService.getRecommendedVenue(clientId, clientSecret, v, latLng);
    }

    public Single<Detail> getDetailofVenue(String path, String clientId, String clientSecret, int v) {
        return apiService.getDetailOfVenue(path, clientId, clientSecret, v);
    }
}
