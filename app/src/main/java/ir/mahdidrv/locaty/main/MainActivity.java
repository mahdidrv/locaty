package ir.mahdidrv.locaty.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.mahdidrv.locaty.R;
import ir.mahdidrv.locaty.model.venue.ItemsItem;
import ir.mahdidrv.locaty.model.venue.Venue;

public class MainActivity extends AppCompatActivity {

    private MainViewModel viewModel;
    private String TAG = "Main";
    List<Venue> venueList;
    private VenueAdapter venueAdapter;
    RecyclerView rv;

    String clientId;
    String clientSecret;
    int v;
    String lngLat;
    String lngLatMzn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        clientId = "1JBOWCC0CYKUML5ZVFQ1HPZSGJ41GZJSF4DQHOTHZH0YH3NK";
        clientSecret = "QQMCGDLRHFG2ZE5323RVQ02OKOUPILTKFWECC1HRUPA5SARI";
        v = 20190425;
        lngLat = "35.75,51.44";
        lngLatMzn = "35.77,51.36";

        viewModel = new MainViewModel();
        venueAdapter = new VenueAdapter();

        setupView();
        observe();

    }

    private void observe() {
        viewModel.getRecommended(clientId, clientSecret, v, lngLat)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Venue>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Venue venue) {
                        venueList = new ArrayList<>();

                        for (ItemsItem i : venue.getResponse().getGroups().get(0).getItems()) {
                            venueList.add(i.getVenue());
                        }

                        Log.i(TAG, "onSuccess: " + venueList.get(2).getName());
                        Log.i(TAG, "onSuccess: " + venueList.get(2).getLocation().getDistance());
                        Log.i(TAG, "onSuccess: " + venueList.get(2).getLocation().getCity());
                        Log.i(TAG, "onSuccess: " + venueList.get(2).getLocation().getState());


                        Log.i(TAG, "onSuccess: " + venueList.size());

                        venueAdapter.setVenues(venueList);
                        rv.setAdapter(venueAdapter);

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError: " + e.getMessage());
                    }
                });
    }

    private void setupView() {
        rv = findViewById(R.id.rv_main);
        rv.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

    }
}
