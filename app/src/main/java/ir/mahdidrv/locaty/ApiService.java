package ir.mahdidrv.locaty;

import io.reactivex.Single;
import ir.mahdidrv.locaty.model.detail.Detail;
import ir.mahdidrv.locaty.model.venue.Venue;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("venues/explore")
    Single<Venue> getRecommendedVenue(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("v") int v,
            @Query("ll") String latLng
    );

    @GET("venues/{venueId}")
    Single<Detail> getDetailOfVenue(
            @Path("venueId") String venueId,
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("v") int v

    );


}
